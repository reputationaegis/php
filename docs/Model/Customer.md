# Customer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_id** | **string** | Customer ID | [optional] 
**companyid** | **string** | Compnay ID | [optional] 
**firstname** | **string** | Customer First Name | [optional] 
**lastname** | **string** | Customer Last Name | [optional] 
**mobile** | **string** | Customer Mobile Phone | [optional] 
**email** | **string** | Customer Email | [optional] 
**email_status** | **string** | Email health status | [optional] 
**is_free_email** | **string** | Is email from free provider ? | [optional] 
**is_typo_fixed** | **string** | Is email corrected ? | [optional] 
**address** | **string** | Customer Postal Address | [optional] 
**zipcode** | **string** | Customer Zipcode | [optional] 
**city** | **string** | Customer City | [optional] 
**state** | **string** | Customer State / Province / Region | [optional] 
**country** | **string** | Customer Country | [optional] 
**language** | **string** | Customer Language | [optional] 
**notes** | **string** | Additional Customer Notes | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


