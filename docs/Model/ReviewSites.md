# ReviewSites

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company_id** | **string** | Company ID | [optional] 
**site_name** | **string** | Site Name | [optional] 
**company_weblinks_url** | **string** | Company Web links Url | [optional] 
**company_weblinks_image_url** | **string** | Company Web links Image Url | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


