# Review

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**review_id** | **string** | Review ID | [optional] 
**company_id** | **string** | Company ID | [optional] 
**review_date** | **string** | Review Date | [optional] 
**review_found_on** | **string** | Review Found On | [optional] 
**company_weblinks_url** | **string** | Company Weblinks Url | [optional] 
**customerid** | **string** | Customer Id | [optional] 
**review_found_on_logo** | **string** | Review Found On Logo | [optional] 
**reviewer_name** | **string** | Reviewer Name | [optional] 
**reviewer_profile_url** | **string** | Reviewer Profile Url | [optional] 
**reviewer_image** | **string** | Reviewer Image | [optional] 
**rating** | **string** | Rating | [optional] 
**rating_image** | **string** | Rating Image | [optional] 
**review_title** | **string** | Review Title | [optional] 
**review_text** | **string** | Review Text | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


