# ReviewSitesReviewCount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**review_site_name** | **string** | Review Site Name | [optional] 
**review_count** | **string** | Review Count | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


