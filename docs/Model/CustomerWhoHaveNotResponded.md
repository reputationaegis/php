# CustomerWhoHaveNotResponded

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_id** | **string** | Customer ID | [optional] 
**customer_name** | **string** | Customer Name | [optional] 
**creation_date** | **string** | Creation Date | [optional] 
**last_sent_date** | **string** | Last Sent Date | [optional] 
**send_method** | **string** | Send Method | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


