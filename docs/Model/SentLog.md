# SentLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sent_log_id** | **string** | Sent Log Id | [optional] 
**company_id** | **string** | Company Id | [optional] 
**customer_id** | **string** | Customer ID | [optional] 
**queue_id** | **string** | Queue ID | [optional] 
**sent_date** | **string** | Sent Date | [optional] 
**send_type** | **string** | Send Type | [optional] 
**send_number** | **string** | Send Number | [optional] 
**send_via** | **string** | Send Via | [optional] 
**email_subject** | **string** | Email Subject | [optional] 
**message** | **string** | Message | [optional] 
**log_status** | **string** | Log Status | [optional] 
**creation_date** | **string** | Creation Date | [optional] 
**open_count** | **string** | Open Count | [optional] 
**latest_open_time** | **string** | Latest Open Time | [optional] 
**click_count** | **string** | Click Count | [optional] 
**latest_click_time** | **string** | Latest Click Time | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


