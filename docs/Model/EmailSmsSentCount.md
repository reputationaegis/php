# EmailSmsSentCount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**emails_sent** | **string** | Emails Sent | [optional] 
**sms_sent** | **string** | SMS Sent | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


