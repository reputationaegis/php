# Queue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**queue_id** | **string** | Queue Id | [optional] 
**company_id** | **string** | Company Id | [optional] 
**customer_id** | **string** | Customer ID | [optional] 
**schedule_date** | **string** | Scheduled Date | [optional] 
**send_type** | **string** | Send Type | [optional] 
**send_number** | **string** | Send Number | [optional] 
**queue_language** | **string** | Queue Language | [optional] 
**email_subject** | **string** | Email Subject | [optional] 
**message** | **string** | Message | [optional] 
**creation_date** | **string** | Creation Date | [optional] 
**published** | **string** | Published | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


