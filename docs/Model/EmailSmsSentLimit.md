# EmailSmsSentLimit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email_limit** | **string** | Email Limit | [optional] 
**sms_limit** | **string** | SMS Limit | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


