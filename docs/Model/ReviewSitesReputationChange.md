# ReviewSitesReputationChange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**review_site_name** | **string** | Review Site Name | [optional] 
**reputation_change** | **string** | ReputationChange | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


