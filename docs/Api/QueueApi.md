# Swagger\Client\QueueApi

All URIs are relative to *https://dev.reputationaegis.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteQueueByCustomerId**](QueueApi.md#deleteQueueByCustomerId) | **DELETE** /deleteQueueByCustomerId | Delete requests of a customer
[**deleteQueueBycompanyId**](QueueApi.md#deleteQueueBycompanyId) | **DELETE** /deleteQueueBycompanyId | Delete requests of a company
[**getQueueBycompanyId**](QueueApi.md#getQueueBycompanyId) | **GET** /getQueueBycompanyId | Get a customer’s queue
[**getSingleCustomerQueue**](QueueApi.md#getSingleCustomerQueue) | **GET** /getQueueByCustomerId | Get a customer’s queue


# **deleteQueueByCustomerId**
> deleteQueueByCustomerId($customerid)

Delete requests of a customer

Delete requests of a customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\QueueApi();
$customerid = "customerid_example"; // string | Customer ID

try {
    $api_instance->deleteQueueByCustomerId($customerid);
} catch (Exception $e) {
    echo 'Exception when calling QueueApi->deleteQueueByCustomerId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerid** | **string**| Customer ID |

### Return type

void (empty response body)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteQueueBycompanyId**
> deleteQueueBycompanyId($companyid)

Delete requests of a company

Delete requests of a company

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\QueueApi();
$companyid = "companyid_example"; // string | Company ID

try {
    $api_instance->deleteQueueBycompanyId($companyid);
} catch (Exception $e) {
    echo 'Exception when calling QueueApi->deleteQueueBycompanyId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |

### Return type

void (empty response body)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getQueueBycompanyId**
> \Swagger\Client\Model\Queue[] getQueueBycompanyId($companyid)

Get a customer’s queue

Get a customer’s queue

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\QueueApi();
$companyid = "companyid_example"; // string | Company ID

try {
    $result = $api_instance->getQueueBycompanyId($companyid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueueApi->getQueueBycompanyId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |

### Return type

[**\Swagger\Client\Model\Queue[]**](../Model/Queue.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSingleCustomerQueue**
> \Swagger\Client\Model\Queue[] getSingleCustomerQueue($customerid)

Get a customer’s queue

Get a customer’s queue

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\QueueApi();
$customerid = "customerid_example"; // string | Customer ID

try {
    $result = $api_instance->getSingleCustomerQueue($customerid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueueApi->getSingleCustomerQueue: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerid** | **string**| Customer ID |

### Return type

[**\Swagger\Client\Model\Queue[]**](../Model/Queue.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

