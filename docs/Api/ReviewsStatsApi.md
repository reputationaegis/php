# Swagger\Client\ReviewsStatsApi

All URIs are relative to *https://dev.reputationaegis.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getNumberOfReviewsPostedInLastDaysByCompanyId**](ReviewsStatsApi.md#getNumberOfReviewsPostedInLastDaysByCompanyId) | **GET** /getNumberOfReviewsPostedInLastDaysByCompanyId | Get total reviews received for each review site
[**getNumberOfReviewsRequiredToAchieve5StarByCompanyId**](ReviewsStatsApi.md#getNumberOfReviewsRequiredToAchieve5StarByCompanyId) | **GET** /getNumberOfReviewsRequiredToAchieve5StarByCompanyId | Get for ALL Review Sites the number of 5-star Feedback / Reviews needed to achieve a 5-star Customer Experience
[**getOverallRatingForEachReviewSiteByCompanyId**](ReviewsStatsApi.md#getOverallRatingForEachReviewSiteByCompanyId) | **GET** /getOverallRatingForEachReviewSiteByCompanyId | Get overall rating for each review site
[**getOverallRatingOfCompanyByCompanyId**](ReviewsStatsApi.md#getOverallRatingOfCompanyByCompanyId) | **GET** /getOverallRatingOfCompanyByCompanyId | Get The Overall Rating of a Company / Location / Feedback Page
[**getOverallReputationChangeOfAllReviewSitesByCompanyId**](ReviewsStatsApi.md#getOverallReputationChangeOfAllReviewSitesByCompanyId) | **GET** /getOverallReputationChangeOfAllReviewSitesByCompanyId | Get the Overall Reputation Increase / Decrease of ALL Review Sites
[**getTotalReviewsReceivedForEachReviewSiteByCompanyId**](ReviewsStatsApi.md#getTotalReviewsReceivedForEachReviewSiteByCompanyId) | **GET** /getTotalReviewsReceivedForEachReviewSiteByCompanyId | Get total reviews received for each review site


# **getNumberOfReviewsPostedInLastDaysByCompanyId**
> \Swagger\Client\Model\ReviewSitesReviewCount[] getNumberOfReviewsPostedInLastDaysByCompanyId($companyid, $last_how_many_days)

Get total reviews received for each review site

Get total reviews received for each review site

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsStatsApi();
$companyid = "companyid_example"; // string | Company ID
$last_how_many_days = "last_how_many_days_example"; // string | Last how many days

try {
    $result = $api_instance->getNumberOfReviewsPostedInLastDaysByCompanyId($companyid, $last_how_many_days);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsStatsApi->getNumberOfReviewsPostedInLastDaysByCompanyId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |
 **last_how_many_days** | **string**| Last how many days |

### Return type

[**\Swagger\Client\Model\ReviewSitesReviewCount[]**](../Model/ReviewSitesReviewCount.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getNumberOfReviewsRequiredToAchieve5StarByCompanyId**
> \Swagger\Client\Model\ReviewSitesReviewCount[] getNumberOfReviewsRequiredToAchieve5StarByCompanyId($companyid)

Get for ALL Review Sites the number of 5-star Feedback / Reviews needed to achieve a 5-star Customer Experience

This API call returns for each Review Site of a Company / Location / Feedback Page, the total number of 5-star Feedback / Reviews that the Company / Location needs, to achieve a 5-star Customer Experience. (NOTE: Most Review Sites display a 5-star image when the Overall Rating of a Company / Location is above 4.51. We decided to work as they do and we display a 5-star image for any Overall Rating above 4.51). URL: https://reputationcrm.com/reputation-control    Please refer this image for more information   https://reputationcrm.com/api/repcrm-api-img/NumberOfReviewsRequiredToAchieve5Star.jpg

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsStatsApi();
$companyid = "companyid_example"; // string | Company ID

try {
    $result = $api_instance->getNumberOfReviewsRequiredToAchieve5StarByCompanyId($companyid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsStatsApi->getNumberOfReviewsRequiredToAchieve5StarByCompanyId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |

### Return type

[**\Swagger\Client\Model\ReviewSitesReviewCount[]**](../Model/ReviewSitesReviewCount.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOverallRatingForEachReviewSiteByCompanyId**
> \Swagger\Client\Model\ReviewSitesRating[] getOverallRatingForEachReviewSiteByCompanyId($companyid)

Get overall rating for each review site

Get overall rating for each review site

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsStatsApi();
$companyid = "companyid_example"; // string | Company ID

try {
    $result = $api_instance->getOverallRatingForEachReviewSiteByCompanyId($companyid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsStatsApi->getOverallRatingForEachReviewSiteByCompanyId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |

### Return type

[**\Swagger\Client\Model\ReviewSitesRating[]**](../Model/ReviewSitesRating.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOverallRatingOfCompanyByCompanyId**
> \Swagger\Client\Model\OverallCompanyRating[] getOverallRatingOfCompanyByCompanyId($companyid)

Get The Overall Rating of a Company / Location / Feedback Page

This API call returns the Overall Rating of a Company / Location / Feedback Page. The number (from 1.00 to 5.00) includes all Feedback / Reviews of a Company / Location / Feedback Page. URL: https://reputationcrm.com/reputation-control    Please refer https://reputationcrm.com/api/repcrm-api-img/OverallCompanyRating.jpg for more information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsStatsApi();
$companyid = "companyid_example"; // string | Company ID

try {
    $result = $api_instance->getOverallRatingOfCompanyByCompanyId($companyid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsStatsApi->getOverallRatingOfCompanyByCompanyId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |

### Return type

[**\Swagger\Client\Model\OverallCompanyRating[]**](../Model/OverallCompanyRating.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOverallReputationChangeOfAllReviewSitesByCompanyId**
> \Swagger\Client\Model\ReviewSitesReputationChange[] getOverallReputationChangeOfAllReviewSitesByCompanyId($companyid)

Get the Overall Reputation Increase / Decrease of ALL Review Sites

This API call returns for each Review Site of a Company / Location / Feedback Page, the Overall Reputation Increase / Decrease of the last 30 days compared to the previous month (31-60 days). 0 means no change: most likely, no new Feedback / Reviews were posted in the past 60 days, or the same number of Feedback / Reviews were posted in the last 30 days compared to the last 31-60 days period. A negative percentage means that the Overall Reputation has suffered. Finally, a positive percentage means an improvement in the Overall Reputation of the Company / Location / Feedback Page. URL: https://reputationcrm.com/reputation-control     Please refer https://reputationcrm.com/api/repcrm-api-img/ReputationChangeInTheLastMonth.jpg for more information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsStatsApi();
$companyid = "companyid_example"; // string | Company ID

try {
    $result = $api_instance->getOverallReputationChangeOfAllReviewSitesByCompanyId($companyid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsStatsApi->getOverallReputationChangeOfAllReviewSitesByCompanyId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |

### Return type

[**\Swagger\Client\Model\ReviewSitesReputationChange[]**](../Model/ReviewSitesReputationChange.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTotalReviewsReceivedForEachReviewSiteByCompanyId**
> \Swagger\Client\Model\ReviewSitesReviewCount[] getTotalReviewsReceivedForEachReviewSiteByCompanyId($companyid)

Get total reviews received for each review site

Get total reviews received for each review site

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsStatsApi();
$companyid = "companyid_example"; // string | Company ID

try {
    $result = $api_instance->getTotalReviewsReceivedForEachReviewSiteByCompanyId($companyid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsStatsApi->getTotalReviewsReceivedForEachReviewSiteByCompanyId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |

### Return type

[**\Swagger\Client\Model\ReviewSitesReviewCount[]**](../Model/ReviewSitesReviewCount.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

