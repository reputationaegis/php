# Swagger\Client\ReviewsApi

All URIs are relative to *https://dev.reputationaegis.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getReviewsByCompanyID**](ReviewsApi.md#getReviewsByCompanyID) | **GET** /getReviewsByCompanyID | Get X last reviews less than X days old of a company
[**getReviewsByCompanyIDAndReviewSite**](ReviewsApi.md#getReviewsByCompanyIDAndReviewSite) | **GET** /getReviewsByCompanyIDAndReviewSite | Get X last reviews less than X days old of a review site of a company
[**getReviewsByCompanyIDRatingAndReviewSite**](ReviewsApi.md#getReviewsByCompanyIDRatingAndReviewSite) | **GET** /getReviewsByCompanyIDRatingAndReviewSite | Get X last reviews that equal or over X stars of a review site of a company
[**getReviewsByCompanyIdAndRating**](ReviewsApi.md#getReviewsByCompanyIdAndRating) | **GET** /getReviewsByCompanyIdAndRating | Get X last reviews that equal or over X stars of a company
[**getReviewsByUserName**](ReviewsApi.md#getReviewsByUserName) | **GET** /getReviewsByUserName | Get X last reviews less than X days old of an account
[**getReviewsByUserNameAndRating**](ReviewsApi.md#getReviewsByUserNameAndRating) | **GET** /getReviewsByUserNameAndRating | Get X last reviews that equal or over X stars of an account
[**getReviewsByUserNameAndReviewSite**](ReviewsApi.md#getReviewsByUserNameAndReviewSite) | **GET** /getReviewsByUserNameAndReviewSite | Get X last reviews less than X days old of a review site of an account
[**getReviewsByUserNameRatingAndReviewSite**](ReviewsApi.md#getReviewsByUserNameRatingAndReviewSite) | **GET** /getReviewsByUserNameRatingAndReviewSite | Get X last reviews that equal or over X stars of a review site of an account
[**postCommentOnReview**](ReviewsApi.md#postCommentOnReview) | **POST** /postCommentOnReview | Comment on a Feedback / Review


# **getReviewsByCompanyID**
> \Swagger\Client\Model\Review[] getReviewsByCompanyID($companyid, $how_many_reviews, $last_how_many_days)

Get X last reviews less than X days old of a company

Get X last reviews less than X days old (For example, last 100 reviews that are less than 365 days old)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsApi();
$companyid = "companyid_example"; // string | Company ID
$how_many_reviews = "how_many_reviews_example"; // string | How many reviews
$last_how_many_days = "last_how_many_days_example"; // string | How many days back

try {
    $result = $api_instance->getReviewsByCompanyID($companyid, $how_many_reviews, $last_how_many_days);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->getReviewsByCompanyID: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |
 **how_many_reviews** | **string**| How many reviews |
 **last_how_many_days** | **string**| How many days back |

### Return type

[**\Swagger\Client\Model\Review[]**](../Model/Review.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getReviewsByCompanyIDAndReviewSite**
> \Swagger\Client\Model\Review[] getReviewsByCompanyIDAndReviewSite($companyid, $how_many_reviews, $last_how_many_days, $reviews_site)

Get X last reviews less than X days old of a review site of a company

Get X last reviews less than X days old of a review site (For example, Google reviews…)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsApi();
$companyid = "companyid_example"; // string | Company ID
$how_many_reviews = "how_many_reviews_example"; // string | How many reviews
$last_how_many_days = "last_how_many_days_example"; // string | How many days back
$reviews_site = "reviews_site_example"; // string | Review site name

try {
    $result = $api_instance->getReviewsByCompanyIDAndReviewSite($companyid, $how_many_reviews, $last_how_many_days, $reviews_site);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->getReviewsByCompanyIDAndReviewSite: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |
 **how_many_reviews** | **string**| How many reviews |
 **last_how_many_days** | **string**| How many days back |
 **reviews_site** | **string**| Review site name |

### Return type

[**\Swagger\Client\Model\Review[]**](../Model/Review.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getReviewsByCompanyIDRatingAndReviewSite**
> \Swagger\Client\Model\Review[] getReviewsByCompanyIDRatingAndReviewSite($companyid, $how_many_reviews, $minimum_ratings, $reviews_site)

Get X last reviews that equal or over X stars of a review site of a company

Get X last reviews that equal or over X stars of a review site (For example, Google reviews…)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsApi();
$companyid = "companyid_example"; // string | Company ID
$how_many_reviews = "how_many_reviews_example"; // string | How many reviews
$minimum_ratings = "minimum_ratings_example"; // string | Minimum review rating (reviews that are equal or over minimumRatings stars)
$reviews_site = "reviews_site_example"; // string | Review site name

try {
    $result = $api_instance->getReviewsByCompanyIDRatingAndReviewSite($companyid, $how_many_reviews, $minimum_ratings, $reviews_site);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->getReviewsByCompanyIDRatingAndReviewSite: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |
 **how_many_reviews** | **string**| How many reviews |
 **minimum_ratings** | **string**| Minimum review rating (reviews that are equal or over minimumRatings stars) |
 **reviews_site** | **string**| Review site name |

### Return type

[**\Swagger\Client\Model\Review[]**](../Model/Review.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getReviewsByCompanyIdAndRating**
> \Swagger\Client\Model\Review[] getReviewsByCompanyIdAndRating($companyid, $how_many_reviews, $last_how_many_days, $minimum_ratings)

Get X last reviews that equal or over X stars of a company

Get X last reviews that equal or over X stars (For example, last 50 reviews that are equal or over 4 stars)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsApi();
$companyid = "companyid_example"; // string | Company ID
$how_many_reviews = "how_many_reviews_example"; // string | How many reviews
$last_how_many_days = "last_how_many_days_example"; // string | How many days back
$minimum_ratings = "minimum_ratings_example"; // string | Minimum review rating (reviews that are equal or over minimumRatings stars)

try {
    $result = $api_instance->getReviewsByCompanyIdAndRating($companyid, $how_many_reviews, $last_how_many_days, $minimum_ratings);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->getReviewsByCompanyIdAndRating: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |
 **how_many_reviews** | **string**| How many reviews |
 **last_how_many_days** | **string**| How many days back |
 **minimum_ratings** | **string**| Minimum review rating (reviews that are equal or over minimumRatings stars) |

### Return type

[**\Swagger\Client\Model\Review[]**](../Model/Review.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getReviewsByUserName**
> \Swagger\Client\Model\Review[] getReviewsByUserName($username, $how_many_reviews, $last_how_many_days)

Get X last reviews less than X days old of an account

Get X last reviews less than X days old (For example, last 100 reviews that are less than 365 days old)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsApi();
$username = "username_example"; // string | User Name or Email address
$how_many_reviews = "how_many_reviews_example"; // string | How many reviews
$last_how_many_days = "last_how_many_days_example"; // string | How many days back

try {
    $result = $api_instance->getReviewsByUserName($username, $how_many_reviews, $last_how_many_days);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->getReviewsByUserName: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| User Name or Email address |
 **how_many_reviews** | **string**| How many reviews |
 **last_how_many_days** | **string**| How many days back |

### Return type

[**\Swagger\Client\Model\Review[]**](../Model/Review.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getReviewsByUserNameAndRating**
> \Swagger\Client\Model\Review[] getReviewsByUserNameAndRating($username, $how_many_reviews, $last_how_many_days, $minimum_ratings)

Get X last reviews that equal or over X stars of an account

Get X last reviews that equal or over X stars (For example, last 50 reviews that are equal or over 4 stars)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsApi();
$username = "username_example"; // string | User Name or Email address
$how_many_reviews = "how_many_reviews_example"; // string | How many reviews
$last_how_many_days = "last_how_many_days_example"; // string | How many days back
$minimum_ratings = "minimum_ratings_example"; // string | Minimum review rating (reviews that are equal or over minimumRatings stars)

try {
    $result = $api_instance->getReviewsByUserNameAndRating($username, $how_many_reviews, $last_how_many_days, $minimum_ratings);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->getReviewsByUserNameAndRating: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| User Name or Email address |
 **how_many_reviews** | **string**| How many reviews |
 **last_how_many_days** | **string**| How many days back |
 **minimum_ratings** | **string**| Minimum review rating (reviews that are equal or over minimumRatings stars) |

### Return type

[**\Swagger\Client\Model\Review[]**](../Model/Review.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getReviewsByUserNameAndReviewSite**
> \Swagger\Client\Model\Review[] getReviewsByUserNameAndReviewSite($username, $how_many_reviews, $last_how_many_days, $reviews_site)

Get X last reviews less than X days old of a review site of an account

Get X last reviews less than X days old of a review site (For Example, Google reviews…)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsApi();
$username = "username_example"; // string | User Name or Email address
$how_many_reviews = "how_many_reviews_example"; // string | How many reviews
$last_how_many_days = "last_how_many_days_example"; // string | How many days back
$reviews_site = "reviews_site_example"; // string | Review site name

try {
    $result = $api_instance->getReviewsByUserNameAndReviewSite($username, $how_many_reviews, $last_how_many_days, $reviews_site);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->getReviewsByUserNameAndReviewSite: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| User Name or Email address |
 **how_many_reviews** | **string**| How many reviews |
 **last_how_many_days** | **string**| How many days back |
 **reviews_site** | **string**| Review site name |

### Return type

[**\Swagger\Client\Model\Review[]**](../Model/Review.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getReviewsByUserNameRatingAndReviewSite**
> \Swagger\Client\Model\Review[] getReviewsByUserNameRatingAndReviewSite($username, $how_many_reviews, $minimum_ratings, $reviews_site)

Get X last reviews that equal or over X stars of a review site of an account

Get X last reviews that equal or over X stars of a review site (For example, Google reviews…)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsApi();
$username = "username_example"; // string | User Name or Email address
$how_many_reviews = "how_many_reviews_example"; // string | How many reviews
$minimum_ratings = "minimum_ratings_example"; // string | Minimum review rating (reviews that are equal or over minimumRatings stars)
$reviews_site = "reviews_site_example"; // string | Review site name

try {
    $result = $api_instance->getReviewsByUserNameRatingAndReviewSite($username, $how_many_reviews, $minimum_ratings, $reviews_site);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->getReviewsByUserNameRatingAndReviewSite: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| User Name or Email address |
 **how_many_reviews** | **string**| How many reviews |
 **minimum_ratings** | **string**| Minimum review rating (reviews that are equal or over minimumRatings stars) |
 **reviews_site** | **string**| Review site name |

### Return type

[**\Swagger\Client\Model\Review[]**](../Model/Review.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **postCommentOnReview**
> postCommentOnReview($reviewid, $commenttext)

Comment on a Feedback / Review

This API call posts a Comment on a Feedback / Review. When posted, the Platform sends an Email to the Commenter / Reviewer with a copy of the posted Comment. NOTE: The Moderation Team receives a copy of the Email sent to the Commenter / Reviewer  The API call has the same effect as using the Reputation Control Center (URL: https://reputationcrm.com/reputation-control), locating in the datatable the Feedback / Review that will be commented, and clicking on the 'Comment on the Platform' button to leave a Comment  Please refer https://reputationcrm.com/api/repcrm-api-img/CommentOnReview.jpg for more information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewsApi();
$reviewid = "reviewid_example"; // string | Review ID
$commenttext = "commenttext_example"; // string | Comment Text

try {
    $api_instance->postCommentOnReview($reviewid, $commenttext);
} catch (Exception $e) {
    echo 'Exception when calling ReviewsApi->postCommentOnReview: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reviewid** | **string**| Review ID |
 **commenttext** | **string**| Comment Text |

### Return type

void (empty response body)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

