# Swagger\Client\CustomerApi

All URIs are relative to *https://dev.reputationaegis.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCustomerAutomatic**](CustomerApi.md#createCustomerAutomatic) | **POST** /createCustomerAutomatic | Create a Customer AND a Feedback / Review Request Sequence with Automatic Scheduling (No delay before sending the First Feedback / Review Request)
[**createCustomerAutomaticWithDelayReviewRequest**](CustomerApi.md#createCustomerAutomaticWithDelayReviewRequest) | **POST** /createCustomerAutomaticWithDelayReviewRequest | Create a Customer AND a Feedback / Review Request Sequence with Automatic Scheduling (Set a Delay before sending the First Feedback / Review Request)
[**createCustomerImmediateFirstReviewRequest**](CustomerApi.md#createCustomerImmediateFirstReviewRequest) | **POST** /createCustomerImmediateFirstReviewRequest | Create a Customer AND Feedback / Review Requests
[**createCustomerManualScheduleOneReviewRequest**](CustomerApi.md#createCustomerManualScheduleOneReviewRequest) | **POST** /createCustomerManualScheduleOneReviewRequest | Create a Customer AND a one Feedback / Review Request (Manual Scheduling)
[**deleteCustomer**](CustomerApi.md#deleteCustomer) | **DELETE** /deleteCustomer | Delete ONE Customer record
[**getAllCustomersBycompanyId**](CustomerApi.md#getAllCustomersBycompanyId) | **GET** /getAllCustomersBycompanyId | Get Information on ALL Customers of a Company / Location / Feedback Page
[**getCustomersWhoHaveNotYetSubmittedReview**](CustomerApi.md#getCustomersWhoHaveNotYetSubmittedReview) | **GET** /getCustomersWhoHaveNotYetSubmittedReview | Get a list of Customers who have not yet submitted their Review / Feedback
[**getSingleCustomer**](CustomerApi.md#getSingleCustomer) | **GET** /getSingleCustomer | Get Information of ONE Customer
[**updateCustomer**](CustomerApi.md#updateCustomer) | **PUT** /updateCustomer | Update the record of ONE Customer


# **createCustomerAutomatic**
> string createCustomerAutomatic($companyid, $firstname, $lastname, $country, $language, $sendtype, $email, $mobile, $address, $zipcode, $city, $state, $notes)

Create a Customer AND a Feedback / Review Request Sequence with Automatic Scheduling (No delay before sending the First Feedback / Review Request)

This API call creates a Customer in Reputation Builder, AND schedules an Automatic Email AND / OR SMS Feedback / Review Request Sequence, WITH NO DELAY BEFORE SENDING THE FIRST FEEDBACK / REVIEW REQUEST, based on settings of the Company / Location -> Notifications -> Email AND SMS Tabs

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\CustomerApi();
$companyid = "companyid_example"; // string | Company ID
$firstname = "firstname_example"; // string | Customer First Name
$lastname = "lastname_example"; // string | Customer Last Name
$country = "country_example"; // string | Customer Country: has to be set to the name of the Country in its original language. Eg: Spain = España. Download the sql file of all countries: https://reputationcrm.com/api/repcrm-api-docs/countries.txt
$language = "language_example"; // string | Customer Language (Language 2 letter ISO code: en, fr, es, it...). For a complete list of supported languages, please check: https://reputationaegis.com/en/features/supported-languages
$sendtype = "1"; // string | Feedback / Review Requests Sending Method (1 = Send via Email, 2 = Send via SMS, 1,2 = Send via Email AND SMS) Allowed values: \"1\", \"2\", \"1,2\"
$email = "email_example"; // string | Customer Email
$mobile = "mobile_example"; // string | Customer Mobile Phone #: The Mobile Phone # must be formatted in its international version. Eg: +33647523335 (+33 = Country Code for France). No leading Zero on Mobile Phone # usually (NOTE: some countries might a leading zero). Here is an excellent script for mobile number formatting: https://github.com/jackocnr/intl-tel-input
$address = "address_example"; // string | Customer Postal Address
$zipcode = "zipcode_example"; // string | Customer Zipcode
$city = "city_example"; // string | Customer City
$state = "state_example"; // string | Customer State / Province / Region
$notes = "notes_example"; // string | Additional Customer Notes

try {
    $result = $api_instance->createCustomerAutomatic($companyid, $firstname, $lastname, $country, $language, $sendtype, $email, $mobile, $address, $zipcode, $city, $state, $notes);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->createCustomerAutomatic: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |
 **firstname** | **string**| Customer First Name |
 **lastname** | **string**| Customer Last Name |
 **country** | **string**| Customer Country: has to be set to the name of the Country in its original language. Eg: Spain &#x3D; España. Download the sql file of all countries: https://reputationcrm.com/api/repcrm-api-docs/countries.txt |
 **language** | **string**| Customer Language (Language 2 letter ISO code: en, fr, es, it...). For a complete list of supported languages, please check: https://reputationaegis.com/en/features/supported-languages |
 **sendtype** | **string**| Feedback / Review Requests Sending Method (1 &#x3D; Send via Email, 2 &#x3D; Send via SMS, 1,2 &#x3D; Send via Email AND SMS) Allowed values: \&quot;1\&quot;, \&quot;2\&quot;, \&quot;1,2\&quot; | [default to 1]
 **email** | **string**| Customer Email | [optional]
 **mobile** | **string**| Customer Mobile Phone #: The Mobile Phone # must be formatted in its international version. Eg: +33647523335 (+33 &#x3D; Country Code for France). No leading Zero on Mobile Phone # usually (NOTE: some countries might a leading zero). Here is an excellent script for mobile number formatting: https://github.com/jackocnr/intl-tel-input | [optional]
 **address** | **string**| Customer Postal Address | [optional]
 **zipcode** | **string**| Customer Zipcode | [optional]
 **city** | **string**| Customer City | [optional]
 **state** | **string**| Customer State / Province / Region | [optional]
 **notes** | **string**| Additional Customer Notes | [optional]

### Return type

**string**

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createCustomerAutomaticWithDelayReviewRequest**
> createCustomerAutomaticWithDelayReviewRequest($companyid, $firstname, $lastname, $country, $language, $sendtype, $delay_before_first_review_request, $email, $mobile, $address, $zipcode, $city, $state, $notes)

Create a Customer AND a Feedback / Review Request Sequence with Automatic Scheduling (Set a Delay before sending the First Feedback / Review Request)

This API call creates a Customer in Reputation Builder, AND schedules an Automatic Email AND / OR SMS Feedback / Review Request Sequence, WITH A DELAY BEFORE SENDING THE FIRST FEEDBACK / REVIEW REQUEST, based on settings of the Company / Location -> Notifications -> Email AND SMS Tabs

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\CustomerApi();
$companyid = "companyid_example"; // string | Company ID
$firstname = "firstname_example"; // string | Customer First Name
$lastname = "lastname_example"; // string | Customer Last Name
$country = "country_example"; // string | Customer Country: has to be set to the name of the Country in its original language. Eg: Spain = España. Download the sql file of all countries: https://reputationcrm.com/api/repcrm-api-docs/countries.txt
$language = "language_example"; // string | Customer Language (Language 2 letter ISO code: en, fr, es, it...). For a complete list of supported languages, please check: https://reputationaegis.com/en/features/supported-languages
$sendtype = "1"; // string | Feedback / Review Requests Sending Method (1 = Send via Email, 2 = Send via SMS, 1,2 = Send via Email AND SMS) Allowed values: \"1\", \"2\", \"1,2\"
$delay_before_first_review_request = "delay_before_first_review_request_example"; // string | Delay before sending the first Feedback / Review Request (Allowed values: \"immediately\", \"X minute\", \"X hour\", \"X day\", \"X week\", \"X month\", \"X year\")
$email = "email_example"; // string | Customer Email
$mobile = "mobile_example"; // string | Customer Mobile Phone #: The Mobile Phone # must be formatted in its international version. Eg: +33647523335 (+33 = Country Code for France). No leading Zero on Mobile Phone # usually (NOTE: some countries might a leading zero). Here is an excellent script for mobile number formatting: https://github.com/jackocnr/intl-tel-input
$address = "address_example"; // string | Customer Postal Address
$zipcode = "zipcode_example"; // string | Customer Zipcode
$city = "city_example"; // string | Customer City
$state = "state_example"; // string | Customer State / Province / Region
$notes = "notes_example"; // string | Additional Customer Notes

try {
    $api_instance->createCustomerAutomaticWithDelayReviewRequest($companyid, $firstname, $lastname, $country, $language, $sendtype, $delay_before_first_review_request, $email, $mobile, $address, $zipcode, $city, $state, $notes);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->createCustomerAutomaticWithDelayReviewRequest: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |
 **firstname** | **string**| Customer First Name |
 **lastname** | **string**| Customer Last Name |
 **country** | **string**| Customer Country: has to be set to the name of the Country in its original language. Eg: Spain &#x3D; España. Download the sql file of all countries: https://reputationcrm.com/api/repcrm-api-docs/countries.txt |
 **language** | **string**| Customer Language (Language 2 letter ISO code: en, fr, es, it...). For a complete list of supported languages, please check: https://reputationaegis.com/en/features/supported-languages |
 **sendtype** | **string**| Feedback / Review Requests Sending Method (1 &#x3D; Send via Email, 2 &#x3D; Send via SMS, 1,2 &#x3D; Send via Email AND SMS) Allowed values: \&quot;1\&quot;, \&quot;2\&quot;, \&quot;1,2\&quot; | [default to 1]
 **delay_before_first_review_request** | **string**| Delay before sending the first Feedback / Review Request (Allowed values: \&quot;immediately\&quot;, \&quot;X minute\&quot;, \&quot;X hour\&quot;, \&quot;X day\&quot;, \&quot;X week\&quot;, \&quot;X month\&quot;, \&quot;X year\&quot;) |
 **email** | **string**| Customer Email | [optional]
 **mobile** | **string**| Customer Mobile Phone #: The Mobile Phone # must be formatted in its international version. Eg: +33647523335 (+33 &#x3D; Country Code for France). No leading Zero on Mobile Phone # usually (NOTE: some countries might a leading zero). Here is an excellent script for mobile number formatting: https://github.com/jackocnr/intl-tel-input | [optional]
 **address** | **string**| Customer Postal Address | [optional]
 **zipcode** | **string**| Customer Zipcode | [optional]
 **city** | **string**| Customer City | [optional]
 **state** | **string**| Customer State / Province / Region | [optional]
 **notes** | **string**| Additional Customer Notes | [optional]

### Return type

void (empty response body)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createCustomerImmediateFirstReviewRequest**
> createCustomerImmediateFirstReviewRequest($companyid, $firstname, $lastname, $country, $language, $sendtype, $email, $mobile, $address, $zipcode, $city, $state, $notes)

Create a Customer AND Feedback / Review Requests

This API call creates a Customer in Reputation Builder AND Feedback / Review Request #1 (Email #1 AND/OR SMS #1 ONLY). Feedback / Review Request(s) are then sent immediately

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\CustomerApi();
$companyid = "companyid_example"; // string | Company ID
$firstname = "firstname_example"; // string | Customer First Name
$lastname = "lastname_example"; // string | Customer Last Name
$country = "country_example"; // string | Customer Country: has to be set to the name of the Country in its original language. Eg: Spain = España. Download the sql file of all countries: https://reputationcrm.com/api/repcrm-api-docs/countries.txt
$language = "language_example"; // string | Customer Language (Language 2 letter ISO code: en, fr, es, it...). For a complete list of supported languages, please check: https://reputationaegis.com/en/features/supported-languages
$sendtype = "1"; // string | Feedback / Review Requests Sending Method (1 = Send via Email, 2 = Send via SMS, 1,2 = Send via Email AND SMS) Allowed values: \"1\", \"2\", \"1,2\"
$email = "email_example"; // string | Customer Email
$mobile = "mobile_example"; // string | Customer Mobile Phone #: The Mobile Phone # must be formatted in its international version. Eg: +33647523335 (+33 = Country Code for France). No leading Zero on Mobile Phone # usually (NOTE: some countries might a leading zero). Here is an excellent script for mobile number formatting: https://github.com/jackocnr/intl-tel-input
$address = "address_example"; // string | Customer Postal Address
$zipcode = "zipcode_example"; // string | Customer Zipcode
$city = "city_example"; // string | Customer City
$state = "state_example"; // string | Customer State / Province / Region
$notes = "notes_example"; // string | Additional Customer Notes

try {
    $api_instance->createCustomerImmediateFirstReviewRequest($companyid, $firstname, $lastname, $country, $language, $sendtype, $email, $mobile, $address, $zipcode, $city, $state, $notes);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->createCustomerImmediateFirstReviewRequest: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |
 **firstname** | **string**| Customer First Name |
 **lastname** | **string**| Customer Last Name |
 **country** | **string**| Customer Country: has to be set to the name of the Country in its original language. Eg: Spain &#x3D; España. Download the sql file of all countries: https://reputationcrm.com/api/repcrm-api-docs/countries.txt |
 **language** | **string**| Customer Language (Language 2 letter ISO code: en, fr, es, it...). For a complete list of supported languages, please check: https://reputationaegis.com/en/features/supported-languages |
 **sendtype** | **string**| Feedback / Review Requests Sending Method (1 &#x3D; Send via Email, 2 &#x3D; Send via SMS, 1,2 &#x3D; Send via Email AND SMS) Allowed values: \&quot;1\&quot;, \&quot;2\&quot;, \&quot;1,2\&quot; | [default to 1]
 **email** | **string**| Customer Email | [optional]
 **mobile** | **string**| Customer Mobile Phone #: The Mobile Phone # must be formatted in its international version. Eg: +33647523335 (+33 &#x3D; Country Code for France). No leading Zero on Mobile Phone # usually (NOTE: some countries might a leading zero). Here is an excellent script for mobile number formatting: https://github.com/jackocnr/intl-tel-input | [optional]
 **address** | **string**| Customer Postal Address | [optional]
 **zipcode** | **string**| Customer Zipcode | [optional]
 **city** | **string**| Customer City | [optional]
 **state** | **string**| Customer State / Province / Region | [optional]
 **notes** | **string**| Additional Customer Notes | [optional]

### Return type

void (empty response body)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createCustomerManualScheduleOneReviewRequest**
> createCustomerManualScheduleOneReviewRequest($companyid, $firstname, $lastname, $country, $language, $sendtype, $emailnumber, $smsnumber, $scheduledate, $scheduletime, $email, $mobile, $address, $zipcode, $city, $state, $notes)

Create a Customer AND a one Feedback / Review Request (Manual Scheduling)

This API call creates a Customer in Reputation Builder AND one Feedback / Review Request. You decide which Feedback / Review Request # to send, the sending method (Email or SMS) and you have full control of the sending date and time

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\CustomerApi();
$companyid = "companyid_example"; // string | Company ID
$firstname = "firstname_example"; // string | Customer First Name
$lastname = "lastname_example"; // string | Customer Last Name
$country = "country_example"; // string | Customer Country: has to be set to the name of the Country in its original language. Eg: Spain = España. Download the sql file of all countries: https://reputationcrm.com/api/repcrm-api-docs/countries.txt
$language = "language_example"; // string | Customer Language (Language 2 letter ISO code: en, fr, es, it...). For a complete list of supported languages, please check: https://reputationaegis.com/en/features/supported-languages
$sendtype = "1"; // string | Feedback / Review Requests Sending Method (1 = Send via Email, 2 = Send via SMS, 1,2 = Send via Email AND SMS) Allowed values: \"1\", \"2\", \"1,2\"
$emailnumber = "1"; // string | Set which Email Feedback / Requests will be sent (Allowed values: \"1\", \"2\", \"3\")
$smsnumber = "1"; // string | Set which SMS Feedback / Requests will be sent (Allowed values: \"1\", \"2\", \"3\")
$scheduledate = "scheduledate_example"; // string | Schedule date (yyyy/mm/dd)
$scheduletime = "scheduletime_example"; // string | Schedule time (HH:MM 24 hours format)
$email = "email_example"; // string | Customer Email
$mobile = "mobile_example"; // string | Customer Mobile Phone #: The Mobile Phone # must be formatted in its international version. Eg: +33647523335 (+33 = Country Code for France). No leading Zero on Mobile Phone # usually (NOTE: some countries might a leading zero). Here is an excellent script for mobile number formatting: https://github.com/jackocnr/intl-tel-input
$address = "address_example"; // string | Customer Postal Address
$zipcode = "zipcode_example"; // string | Customer Zipcode
$city = "city_example"; // string | Customer City
$state = "state_example"; // string | Customer State / Province / Region
$notes = "notes_example"; // string | Additional Customer Notes

try {
    $api_instance->createCustomerManualScheduleOneReviewRequest($companyid, $firstname, $lastname, $country, $language, $sendtype, $emailnumber, $smsnumber, $scheduledate, $scheduletime, $email, $mobile, $address, $zipcode, $city, $state, $notes);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->createCustomerManualScheduleOneReviewRequest: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |
 **firstname** | **string**| Customer First Name |
 **lastname** | **string**| Customer Last Name |
 **country** | **string**| Customer Country: has to be set to the name of the Country in its original language. Eg: Spain &#x3D; España. Download the sql file of all countries: https://reputationcrm.com/api/repcrm-api-docs/countries.txt |
 **language** | **string**| Customer Language (Language 2 letter ISO code: en, fr, es, it...). For a complete list of supported languages, please check: https://reputationaegis.com/en/features/supported-languages |
 **sendtype** | **string**| Feedback / Review Requests Sending Method (1 &#x3D; Send via Email, 2 &#x3D; Send via SMS, 1,2 &#x3D; Send via Email AND SMS) Allowed values: \&quot;1\&quot;, \&quot;2\&quot;, \&quot;1,2\&quot; | [default to 1]
 **emailnumber** | **string**| Set which Email Feedback / Requests will be sent (Allowed values: \&quot;1\&quot;, \&quot;2\&quot;, \&quot;3\&quot;) | [default to 1]
 **smsnumber** | **string**| Set which SMS Feedback / Requests will be sent (Allowed values: \&quot;1\&quot;, \&quot;2\&quot;, \&quot;3\&quot;) | [default to 1]
 **scheduledate** | **string**| Schedule date (yyyy/mm/dd) |
 **scheduletime** | **string**| Schedule time (HH:MM 24 hours format) |
 **email** | **string**| Customer Email | [optional]
 **mobile** | **string**| Customer Mobile Phone #: The Mobile Phone # must be formatted in its international version. Eg: +33647523335 (+33 &#x3D; Country Code for France). No leading Zero on Mobile Phone # usually (NOTE: some countries might a leading zero). Here is an excellent script for mobile number formatting: https://github.com/jackocnr/intl-tel-input | [optional]
 **address** | **string**| Customer Postal Address | [optional]
 **zipcode** | **string**| Customer Zipcode | [optional]
 **city** | **string**| Customer City | [optional]
 **state** | **string**| Customer State / Province / Region | [optional]
 **notes** | **string**| Additional Customer Notes | [optional]

### Return type

void (empty response body)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteCustomer**
> deleteCustomer($customerid)

Delete ONE Customer record

This API call deletes an existing Customer from Reputation Builder. It will also delete all Email / SMS Requests still in the Queue

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\CustomerApi();
$customerid = "customerid_example"; // string | Customer ID

try {
    $api_instance->deleteCustomer($customerid);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->deleteCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerid** | **string**| Customer ID |

### Return type

void (empty response body)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllCustomersBycompanyId**
> \Swagger\Client\Model\Customer[] getAllCustomersBycompanyId($companyid)

Get Information on ALL Customers of a Company / Location / Feedback Page

This API call returns information from Reputation Builder on ALL existing Customers of a Company / Location / Feedback Page

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\CustomerApi();
$companyid = "companyid_example"; // string | Company ID

try {
    $result = $api_instance->getAllCustomersBycompanyId($companyid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->getAllCustomersBycompanyId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |

### Return type

[**\Swagger\Client\Model\Customer[]**](../Model/Customer.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCustomersWhoHaveNotYetSubmittedReview**
> \Swagger\Client\Model\CustomerWhoHaveNotResponded[] getCustomersWhoHaveNotYetSubmittedReview($companyid)

Get a list of Customers who have not yet submitted their Review / Feedback

This API call returns the list of Customers of a Company / Location / Feedback Page who have not yet responded to Feedback / Review Requests

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\CustomerApi();
$companyid = "companyid_example"; // string | Company ID

try {
    $result = $api_instance->getCustomersWhoHaveNotYetSubmittedReview($companyid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->getCustomersWhoHaveNotYetSubmittedReview: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |

### Return type

[**\Swagger\Client\Model\CustomerWhoHaveNotResponded[]**](../Model/CustomerWhoHaveNotResponded.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSingleCustomer**
> \Swagger\Client\Model\Customer[] getSingleCustomer($customerid)

Get Information of ONE Customer

This API call returns information from Reputation Builder on ONE existing Customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\CustomerApi();
$customerid = "customerid_example"; // string | Customer ID

try {
    $result = $api_instance->getSingleCustomer($customerid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->getSingleCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerid** | **string**| Customer ID |

### Return type

[**\Swagger\Client\Model\Customer[]**](../Model/Customer.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateCustomer**
> updateCustomer($customerid, $firstname, $lastname, $country, $language, $notes, $email, $mobile, $address, $zipcode, $city, $state)

Update the record of ONE Customer

This API call updates information from Reputation Builder on ONE existing Customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\CustomerApi();
$customerid = "customerid_example"; // string | Customer ID
$firstname = "firstname_example"; // string | Customer First Name
$lastname = "lastname_example"; // string | Customer Last Name
$country = "country_example"; // string | Customer Country
$language = "language_example"; // string | Customer Language
$notes = "notes_example"; // string | Additional Customer Notes
$email = "email_example"; // string | Customer Email
$mobile = "mobile_example"; // string | Customer Mobile Phone
$address = "address_example"; // string | Customer Postal Address
$zipcode = "zipcode_example"; // string | Customer Zipcode
$city = "city_example"; // string | Customer City
$state = "state_example"; // string | Customer State / Province / Region

try {
    $api_instance->updateCustomer($customerid, $firstname, $lastname, $country, $language, $notes, $email, $mobile, $address, $zipcode, $city, $state);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->updateCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerid** | **string**| Customer ID |
 **firstname** | **string**| Customer First Name |
 **lastname** | **string**| Customer Last Name |
 **country** | **string**| Customer Country |
 **language** | **string**| Customer Language |
 **notes** | **string**| Additional Customer Notes |
 **email** | **string**| Customer Email | [optional]
 **mobile** | **string**| Customer Mobile Phone | [optional]
 **address** | **string**| Customer Postal Address | [optional]
 **zipcode** | **string**| Customer Zipcode | [optional]
 **city** | **string**| Customer City | [optional]
 **state** | **string**| Customer State / Province / Region | [optional]

### Return type

void (empty response body)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

