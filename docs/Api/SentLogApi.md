# Swagger\Client\SentLogApi

All URIs are relative to *https://dev.reputationaegis.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getSentLogByCompanyId**](SentLogApi.md#getSentLogByCompanyId) | **GET** /getSentLogByCompanyId | Get company&#39;s sent log
[**getSentLogByCustomerId**](SentLogApi.md#getSentLogByCustomerId) | **GET** /getSentLogByCustomerId | Get a customer’s sent log


# **getSentLogByCompanyId**
> \Swagger\Client\Model\SentLog[] getSentLogByCompanyId($companyid)

Get company's sent log

Get company's sent log

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\SentLogApi();
$companyid = "companyid_example"; // string | Company ID

try {
    $result = $api_instance->getSentLogByCompanyId($companyid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SentLogApi->getSentLogByCompanyId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |

### Return type

[**\Swagger\Client\Model\SentLog[]**](../Model/SentLog.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSentLogByCustomerId**
> \Swagger\Client\Model\SentLog[] getSentLogByCustomerId($customerid)

Get a customer’s sent log

Get a customer’s sent log

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\SentLogApi();
$customerid = "customerid_example"; // string | Customer ID

try {
    $result = $api_instance->getSentLogByCustomerId($customerid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SentLogApi->getSentLogByCustomerId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerid** | **string**| Customer ID |

### Return type

[**\Swagger\Client\Model\SentLog[]**](../Model/SentLog.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

