# Swagger\Client\EmailSMSStatsApi

All URIs are relative to *https://dev.reputationaegis.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getEmailSmsSentCount**](EmailSMSStatsApi.md#getEmailSmsSentCount) | **GET** /getEmailSmsSentCount | Email / SMS sent
[**getEmailSmsSentLimit**](EmailSMSStatsApi.md#getEmailSmsSentLimit) | **GET** /getEmailSmsSentLimit | Email / SMS Limit


# **getEmailSmsSentCount**
> \Swagger\Client\Model\EmailSmsSentCount[] getEmailSmsSentCount($username)

Email / SMS sent

Email / SMS sent

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\EmailSMSStatsApi();
$username = "username_example"; // string | User Name or Email address

try {
    $result = $api_instance->getEmailSmsSentCount($username);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EmailSMSStatsApi->getEmailSmsSentCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| User Name or Email address |

### Return type

[**\Swagger\Client\Model\EmailSmsSentCount[]**](../Model/EmailSmsSentCount.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEmailSmsSentLimit**
> \Swagger\Client\Model\EmailSmsSentLimit[] getEmailSmsSentLimit($username)

Email / SMS Limit

Email / SMS Limit

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\EmailSMSStatsApi();
$username = "username_example"; // string | User Name or Email address

try {
    $result = $api_instance->getEmailSmsSentLimit($username);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EmailSMSStatsApi->getEmailSmsSentLimit: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| User Name or Email address |

### Return type

[**\Swagger\Client\Model\EmailSmsSentLimit[]**](../Model/EmailSmsSentLimit.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

