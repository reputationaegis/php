# Swagger\Client\ReviewSitesApi

All URIs are relative to *https://dev.reputationaegis.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllReviewSitesByUserName**](ReviewSitesApi.md#getAllReviewSitesByUserName) | **GET** /getAllReviewSitesByUserName | Get all review sites of an account
[**getAllReviewSitesBycompanyId**](ReviewSitesApi.md#getAllReviewSitesBycompanyId) | **GET** /getAllReviewSitesBycompanyId | Get all review sites of a company


# **getAllReviewSitesByUserName**
> \Swagger\Client\Model\ReviewSites[] getAllReviewSitesByUserName($username)

Get all review sites of an account

Get all review sites of an account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewSitesApi();
$username = "username_example"; // string | User Name or Email address

try {
    $result = $api_instance->getAllReviewSitesByUserName($username);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewSitesApi->getAllReviewSitesByUserName: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| User Name or Email address |

### Return type

[**\Swagger\Client\Model\ReviewSites[]**](../Model/ReviewSites.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllReviewSitesBycompanyId**
> \Swagger\Client\Model\ReviewSites[] getAllReviewSitesBycompanyId($companyid)

Get all review sites of a company

Get all review sites of a company

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\ReviewSitesApi();
$companyid = "companyid_example"; // string | Company ID

try {
    $result = $api_instance->getAllReviewSitesBycompanyId($companyid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReviewSitesApi->getAllReviewSitesBycompanyId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **companyid** | **string**| Company ID |

### Return type

[**\Swagger\Client\Model\ReviewSites[]**](../Model/ReviewSites.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

