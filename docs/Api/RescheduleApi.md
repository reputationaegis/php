# Swagger\Client\RescheduleApi

All URIs are relative to *https://dev.reputationaegis.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rescheduleCustomerAutomatic**](RescheduleApi.md#rescheduleCustomerAutomatic) | **POST** /rescheduleCustomerAutomatic | Reschedule Customer (Automatic)
[**rescheduleCustomerImmediate**](RescheduleApi.md#rescheduleCustomerImmediate) | **POST** /rescheduleCustomerImmediate | Reschedule Customer (Send right away)
[**rescheduleManualOneReviewRequest**](RescheduleApi.md#rescheduleManualOneReviewRequest) | **POST** /rescheduleManualOneReviewRequest | Reschedule Customer (Manual Schedule)


# **rescheduleCustomerAutomatic**
> rescheduleCustomerAutomatic($customerid, $language)

Reschedule Customer (Automatic)

Reschedule Customer (Automatic)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\RescheduleApi();
$customerid = "customerid_example"; // string | Customer ID
$language = "language_example"; // string | Customer Language (Language 2 letter ISO code: en, fr, es, it...). For a complete list of supported languages, please check: https://reputationaegis.com/en/features/supported-languages

try {
    $api_instance->rescheduleCustomerAutomatic($customerid, $language);
} catch (Exception $e) {
    echo 'Exception when calling RescheduleApi->rescheduleCustomerAutomatic: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerid** | **string**| Customer ID |
 **language** | **string**| Customer Language (Language 2 letter ISO code: en, fr, es, it...). For a complete list of supported languages, please check: https://reputationaegis.com/en/features/supported-languages |

### Return type

void (empty response body)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rescheduleCustomerImmediate**
> rescheduleCustomerImmediate($customerid, $language)

Reschedule Customer (Send right away)

Reschedule Customer (Send right away)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\RescheduleApi();
$customerid = "customerid_example"; // string | Customer ID
$language = "language_example"; // string | Customer Language (Language 2 letter ISO code: en, fr, es, it...). For a complete list of supported languages, please check: https://reputationaegis.com/en/features/supported-languages

try {
    $api_instance->rescheduleCustomerImmediate($customerid, $language);
} catch (Exception $e) {
    echo 'Exception when calling RescheduleApi->rescheduleCustomerImmediate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerid** | **string**| Customer ID |
 **language** | **string**| Customer Language (Language 2 letter ISO code: en, fr, es, it...). For a complete list of supported languages, please check: https://reputationaegis.com/en/features/supported-languages |

### Return type

void (empty response body)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **rescheduleManualOneReviewRequest**
> rescheduleManualOneReviewRequest($customerid, $language, $scheduledate, $scheduletime, $emailnumber, $smsnumber)

Reschedule Customer (Manual Schedule)

Reschedule Customer (Manual Schedule)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('key', 'Bearer');

$api_instance = new Swagger\Client\Api\RescheduleApi();
$customerid = "customerid_example"; // string | Customer ID
$language = "language_example"; // string | Customer Language (Language 2 letter ISO code: en, fr, es, it...). For a complete list of supported languages, please check: https://reputationaegis.com/en/features/supported-languages
$scheduledate = "scheduledate_example"; // string | Schedule date (yyyy/mm/dd)
$scheduletime = "scheduletime_example"; // string | Schedule time (HH:MM 24 hours format)
$emailnumber = "1"; // string | Set which Email Feedback / Requests will be sent (Allowed values: \"1\", \"2\", \"3\")
$smsnumber = "1"; // string | Set which SMS Feedback / Requests will be sent (Allowed values: \"1\", \"2\", \"3\")

try {
    $api_instance->rescheduleManualOneReviewRequest($customerid, $language, $scheduledate, $scheduletime, $emailnumber, $smsnumber);
} catch (Exception $e) {
    echo 'Exception when calling RescheduleApi->rescheduleManualOneReviewRequest: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customerid** | **string**| Customer ID |
 **language** | **string**| Customer Language (Language 2 letter ISO code: en, fr, es, it...). For a complete list of supported languages, please check: https://reputationaegis.com/en/features/supported-languages |
 **scheduledate** | **string**| Schedule date (yyyy/mm/dd) |
 **scheduletime** | **string**| Schedule time (HH:MM 24 hours format) |
 **emailnumber** | **string**| Set which Email Feedback / Requests will be sent (Allowed values: \&quot;1\&quot;, \&quot;2\&quot;, \&quot;3\&quot;) | [default to 1]
 **smsnumber** | **string**| Set which SMS Feedback / Requests will be sent (Allowed values: \&quot;1\&quot;, \&quot;2\&quot;, \&quot;3\&quot;) | [default to 1]

### Return type

void (empty response body)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

