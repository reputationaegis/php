<?php
/**
 * CustomerTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Reputation Aegis API
 *
 * Move your app forward with the Reputation Aegis API  To get your `API key`, visit [Reputation Aegis](https://reputationcrm.com/settings/index/Reputation-Builder)  Your API Key can be found in Settings -> Reputation Builder -> Reputation Builder -> API Tab
 *
 * OpenAPI spec version: 1.0.0
 * Contact: support@reputationcrm.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * CustomerTest Class Doc Comment
 *
 * @category    Class */
// * @description Customer
/**
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CustomerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test "Customer"
     */
    public function testCustomer()
    {

    }

    /**
     * Test attribute "customer_id"
     */
    public function testPropertyCustomerId()
    {

    }

    /**
     * Test attribute "companyid"
     */
    public function testPropertyCompanyid()
    {

    }

    /**
     * Test attribute "firstname"
     */
    public function testPropertyFirstname()
    {

    }

    /**
     * Test attribute "lastname"
     */
    public function testPropertyLastname()
    {

    }

    /**
     * Test attribute "mobile"
     */
    public function testPropertyMobile()
    {

    }

    /**
     * Test attribute "email"
     */
    public function testPropertyEmail()
    {

    }

    /**
     * Test attribute "email_status"
     */
    public function testPropertyEmailStatus()
    {

    }

    /**
     * Test attribute "is_free_email"
     */
    public function testPropertyIsFreeEmail()
    {

    }

    /**
     * Test attribute "is_typo_fixed"
     */
    public function testPropertyIsTypoFixed()
    {

    }

    /**
     * Test attribute "address"
     */
    public function testPropertyAddress()
    {

    }

    /**
     * Test attribute "zipcode"
     */
    public function testPropertyZipcode()
    {

    }

    /**
     * Test attribute "city"
     */
    public function testPropertyCity()
    {

    }

    /**
     * Test attribute "state"
     */
    public function testPropertyState()
    {

    }

    /**
     * Test attribute "country"
     */
    public function testPropertyCountry()
    {

    }

    /**
     * Test attribute "language"
     */
    public function testPropertyLanguage()
    {

    }

    /**
     * Test attribute "notes"
     */
    public function testPropertyNotes()
    {

    }

}
