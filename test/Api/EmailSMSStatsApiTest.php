<?php
/**
 * EmailSMSStatsApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Reputation Aegis API
 *
 * Move your app forward with the Reputation Aegis API  To get your `API key`, visit [Reputation Aegis](https://reputationcrm.com/settings/index/Reputation-Builder)  Your API Key can be found in Settings -> Reputation Builder -> Reputation Builder -> API Tab
 *
 * OpenAPI spec version: 1.0.0
 * Contact: support@reputationcrm.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Swagger\Client;

use \Swagger\Client\Configuration;
use \Swagger\Client\ApiClient;
use \Swagger\Client\ApiException;
use \Swagger\Client\ObjectSerializer;

/**
 * EmailSMSStatsApiTest Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class EmailSMSStatsApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test case for getEmailSmsSentCount
     *
     * Email / SMS sent.
     *
     */
    public function testGetEmailSmsSentCount()
    {

    }

    /**
     * Test case for getEmailSmsSentLimit
     *
     * Email / SMS Limit.
     *
     */
    public function testGetEmailSmsSentLimit()
    {

    }

}
