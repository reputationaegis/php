<?php
/**
 * ReviewSites
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Reputation Aegis API
 *
 * Move your app forward with the Reputation Aegis API  To get your `API key`, visit [Reputation Aegis](https://reputationcrm.com/settings/index/Reputation-Builder)  Your API Key can be found in Settings -> Reputation Builder -> Reputation Builder -> API Tab
 *
 * OpenAPI spec version: 1.0.0
 * Contact: support@reputationcrm.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * ReviewSites Class Doc Comment
 *
 * @category    Class */
/**
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ReviewSites implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'ReviewSites';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'company_id' => 'string',
        'site_name' => 'string',
        'company_weblinks_url' => 'string',
        'company_weblinks_image_url' => 'string'
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'company_id' => 'companyID',
        'site_name' => 'SiteName',
        'company_weblinks_url' => 'CompanyWeblinksUrl',
        'company_weblinks_image_url' => 'CompanyWeblinksImageUrl'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'company_id' => 'setCompanyId',
        'site_name' => 'setSiteName',
        'company_weblinks_url' => 'setCompanyWeblinksUrl',
        'company_weblinks_image_url' => 'setCompanyWeblinksImageUrl'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'company_id' => 'getCompanyId',
        'site_name' => 'getSiteName',
        'company_weblinks_url' => 'getCompanyWeblinksUrl',
        'company_weblinks_image_url' => 'getCompanyWeblinksImageUrl'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['company_id'] = isset($data['company_id']) ? $data['company_id'] : null;
        $this->container['site_name'] = isset($data['site_name']) ? $data['site_name'] : null;
        $this->container['company_weblinks_url'] = isset($data['company_weblinks_url']) ? $data['company_weblinks_url'] : null;
        $this->container['company_weblinks_image_url'] = isset($data['company_weblinks_image_url']) ? $data['company_weblinks_image_url'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        return true;
    }


    /**
     * Gets company_id
     * @return string
     */
    public function getCompanyId()
    {
        return $this->container['company_id'];
    }

    /**
     * Sets company_id
     * @param string $company_id Company ID
     * @return $this
     */
    public function setCompanyId($company_id)
    {
        $this->container['company_id'] = $company_id;

        return $this;
    }

    /**
     * Gets site_name
     * @return string
     */
    public function getSiteName()
    {
        return $this->container['site_name'];
    }

    /**
     * Sets site_name
     * @param string $site_name Site Name
     * @return $this
     */
    public function setSiteName($site_name)
    {
        $this->container['site_name'] = $site_name;

        return $this;
    }

    /**
     * Gets company_weblinks_url
     * @return string
     */
    public function getCompanyWeblinksUrl()
    {
        return $this->container['company_weblinks_url'];
    }

    /**
     * Sets company_weblinks_url
     * @param string $company_weblinks_url Company Web links Url
     * @return $this
     */
    public function setCompanyWeblinksUrl($company_weblinks_url)
    {
        $this->container['company_weblinks_url'] = $company_weblinks_url;

        return $this;
    }

    /**
     * Gets company_weblinks_image_url
     * @return string
     */
    public function getCompanyWeblinksImageUrl()
    {
        return $this->container['company_weblinks_image_url'];
    }

    /**
     * Sets company_weblinks_image_url
     * @param string $company_weblinks_image_url Company Web links Image Url
     * @return $this
     */
    public function setCompanyWeblinksImageUrl($company_weblinks_image_url)
    {
        $this->container['company_weblinks_image_url'] = $company_weblinks_image_url;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


