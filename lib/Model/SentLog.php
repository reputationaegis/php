<?php
/**
 * SentLog
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Reputation Aegis API
 *
 * Move your app forward with the Reputation Aegis API  To get your `API key`, visit [Reputation Aegis](https://reputationcrm.com/settings/index/Reputation-Builder)  Your API Key can be found in Settings -> Reputation Builder -> Reputation Builder -> API Tab
 *
 * OpenAPI spec version: 1.0.0
 * Contact: support@reputationcrm.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * SentLog Class Doc Comment
 *
 * @category    Class */
/**
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class SentLog implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'SentLog';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'sent_log_id' => 'string',
        'company_id' => 'string',
        'customer_id' => 'string',
        'queue_id' => 'string',
        'sent_date' => 'string',
        'send_type' => 'string',
        'send_number' => 'string',
        'send_via' => 'string',
        'email_subject' => 'string',
        'message' => 'string',
        'log_status' => 'string',
        'creation_date' => 'string',
        'open_count' => 'string',
        'latest_open_time' => 'string',
        'click_count' => 'string',
        'latest_click_time' => 'string'
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'sent_log_id' => 'sentLogId',
        'company_id' => 'companyId',
        'customer_id' => 'customerID',
        'queue_id' => 'queueID',
        'sent_date' => 'SentDate',
        'send_type' => 'SendType',
        'send_number' => 'SendNumber',
        'send_via' => 'SendVia',
        'email_subject' => 'EmailSubject',
        'message' => 'Message',
        'log_status' => 'LogStatus',
        'creation_date' => 'CreationDate',
        'open_count' => 'OpenCount',
        'latest_open_time' => 'LatestOpenTime',
        'click_count' => 'ClickCount',
        'latest_click_time' => 'LatestClickTime'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'sent_log_id' => 'setSentLogId',
        'company_id' => 'setCompanyId',
        'customer_id' => 'setCustomerId',
        'queue_id' => 'setQueueId',
        'sent_date' => 'setSentDate',
        'send_type' => 'setSendType',
        'send_number' => 'setSendNumber',
        'send_via' => 'setSendVia',
        'email_subject' => 'setEmailSubject',
        'message' => 'setMessage',
        'log_status' => 'setLogStatus',
        'creation_date' => 'setCreationDate',
        'open_count' => 'setOpenCount',
        'latest_open_time' => 'setLatestOpenTime',
        'click_count' => 'setClickCount',
        'latest_click_time' => 'setLatestClickTime'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'sent_log_id' => 'getSentLogId',
        'company_id' => 'getCompanyId',
        'customer_id' => 'getCustomerId',
        'queue_id' => 'getQueueId',
        'sent_date' => 'getSentDate',
        'send_type' => 'getSendType',
        'send_number' => 'getSendNumber',
        'send_via' => 'getSendVia',
        'email_subject' => 'getEmailSubject',
        'message' => 'getMessage',
        'log_status' => 'getLogStatus',
        'creation_date' => 'getCreationDate',
        'open_count' => 'getOpenCount',
        'latest_open_time' => 'getLatestOpenTime',
        'click_count' => 'getClickCount',
        'latest_click_time' => 'getLatestClickTime'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['sent_log_id'] = isset($data['sent_log_id']) ? $data['sent_log_id'] : null;
        $this->container['company_id'] = isset($data['company_id']) ? $data['company_id'] : null;
        $this->container['customer_id'] = isset($data['customer_id']) ? $data['customer_id'] : null;
        $this->container['queue_id'] = isset($data['queue_id']) ? $data['queue_id'] : null;
        $this->container['sent_date'] = isset($data['sent_date']) ? $data['sent_date'] : null;
        $this->container['send_type'] = isset($data['send_type']) ? $data['send_type'] : null;
        $this->container['send_number'] = isset($data['send_number']) ? $data['send_number'] : null;
        $this->container['send_via'] = isset($data['send_via']) ? $data['send_via'] : null;
        $this->container['email_subject'] = isset($data['email_subject']) ? $data['email_subject'] : null;
        $this->container['message'] = isset($data['message']) ? $data['message'] : null;
        $this->container['log_status'] = isset($data['log_status']) ? $data['log_status'] : null;
        $this->container['creation_date'] = isset($data['creation_date']) ? $data['creation_date'] : null;
        $this->container['open_count'] = isset($data['open_count']) ? $data['open_count'] : null;
        $this->container['latest_open_time'] = isset($data['latest_open_time']) ? $data['latest_open_time'] : null;
        $this->container['click_count'] = isset($data['click_count']) ? $data['click_count'] : null;
        $this->container['latest_click_time'] = isset($data['latest_click_time']) ? $data['latest_click_time'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        return true;
    }


    /**
     * Gets sent_log_id
     * @return string
     */
    public function getSentLogId()
    {
        return $this->container['sent_log_id'];
    }

    /**
     * Sets sent_log_id
     * @param string $sent_log_id Sent Log Id
     * @return $this
     */
    public function setSentLogId($sent_log_id)
    {
        $this->container['sent_log_id'] = $sent_log_id;

        return $this;
    }

    /**
     * Gets company_id
     * @return string
     */
    public function getCompanyId()
    {
        return $this->container['company_id'];
    }

    /**
     * Sets company_id
     * @param string $company_id Company Id
     * @return $this
     */
    public function setCompanyId($company_id)
    {
        $this->container['company_id'] = $company_id;

        return $this;
    }

    /**
     * Gets customer_id
     * @return string
     */
    public function getCustomerId()
    {
        return $this->container['customer_id'];
    }

    /**
     * Sets customer_id
     * @param string $customer_id Customer ID
     * @return $this
     */
    public function setCustomerId($customer_id)
    {
        $this->container['customer_id'] = $customer_id;

        return $this;
    }

    /**
     * Gets queue_id
     * @return string
     */
    public function getQueueId()
    {
        return $this->container['queue_id'];
    }

    /**
     * Sets queue_id
     * @param string $queue_id Queue ID
     * @return $this
     */
    public function setQueueId($queue_id)
    {
        $this->container['queue_id'] = $queue_id;

        return $this;
    }

    /**
     * Gets sent_date
     * @return string
     */
    public function getSentDate()
    {
        return $this->container['sent_date'];
    }

    /**
     * Sets sent_date
     * @param string $sent_date Sent Date
     * @return $this
     */
    public function setSentDate($sent_date)
    {
        $this->container['sent_date'] = $sent_date;

        return $this;
    }

    /**
     * Gets send_type
     * @return string
     */
    public function getSendType()
    {
        return $this->container['send_type'];
    }

    /**
     * Sets send_type
     * @param string $send_type Send Type
     * @return $this
     */
    public function setSendType($send_type)
    {
        $this->container['send_type'] = $send_type;

        return $this;
    }

    /**
     * Gets send_number
     * @return string
     */
    public function getSendNumber()
    {
        return $this->container['send_number'];
    }

    /**
     * Sets send_number
     * @param string $send_number Send Number
     * @return $this
     */
    public function setSendNumber($send_number)
    {
        $this->container['send_number'] = $send_number;

        return $this;
    }

    /**
     * Gets send_via
     * @return string
     */
    public function getSendVia()
    {
        return $this->container['send_via'];
    }

    /**
     * Sets send_via
     * @param string $send_via Send Via
     * @return $this
     */
    public function setSendVia($send_via)
    {
        $this->container['send_via'] = $send_via;

        return $this;
    }

    /**
     * Gets email_subject
     * @return string
     */
    public function getEmailSubject()
    {
        return $this->container['email_subject'];
    }

    /**
     * Sets email_subject
     * @param string $email_subject Email Subject
     * @return $this
     */
    public function setEmailSubject($email_subject)
    {
        $this->container['email_subject'] = $email_subject;

        return $this;
    }

    /**
     * Gets message
     * @return string
     */
    public function getMessage()
    {
        return $this->container['message'];
    }

    /**
     * Sets message
     * @param string $message Message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->container['message'] = $message;

        return $this;
    }

    /**
     * Gets log_status
     * @return string
     */
    public function getLogStatus()
    {
        return $this->container['log_status'];
    }

    /**
     * Sets log_status
     * @param string $log_status Log Status
     * @return $this
     */
    public function setLogStatus($log_status)
    {
        $this->container['log_status'] = $log_status;

        return $this;
    }

    /**
     * Gets creation_date
     * @return string
     */
    public function getCreationDate()
    {
        return $this->container['creation_date'];
    }

    /**
     * Sets creation_date
     * @param string $creation_date Creation Date
     * @return $this
     */
    public function setCreationDate($creation_date)
    {
        $this->container['creation_date'] = $creation_date;

        return $this;
    }

    /**
     * Gets open_count
     * @return string
     */
    public function getOpenCount()
    {
        return $this->container['open_count'];
    }

    /**
     * Sets open_count
     * @param string $open_count Open Count
     * @return $this
     */
    public function setOpenCount($open_count)
    {
        $this->container['open_count'] = $open_count;

        return $this;
    }

    /**
     * Gets latest_open_time
     * @return string
     */
    public function getLatestOpenTime()
    {
        return $this->container['latest_open_time'];
    }

    /**
     * Sets latest_open_time
     * @param string $latest_open_time Latest Open Time
     * @return $this
     */
    public function setLatestOpenTime($latest_open_time)
    {
        $this->container['latest_open_time'] = $latest_open_time;

        return $this;
    }

    /**
     * Gets click_count
     * @return string
     */
    public function getClickCount()
    {
        return $this->container['click_count'];
    }

    /**
     * Sets click_count
     * @param string $click_count Click Count
     * @return $this
     */
    public function setClickCount($click_count)
    {
        $this->container['click_count'] = $click_count;

        return $this;
    }

    /**
     * Gets latest_click_time
     * @return string
     */
    public function getLatestClickTime()
    {
        return $this->container['latest_click_time'];
    }

    /**
     * Sets latest_click_time
     * @param string $latest_click_time Latest Click Time
     * @return $this
     */
    public function setLatestClickTime($latest_click_time)
    {
        $this->container['latest_click_time'] = $latest_click_time;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


